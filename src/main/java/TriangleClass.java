public class TriangleClass extends Figure{

    public TriangleClass(){     //Пустой конструктор
        System.out.println();
        System.out.println("New Triangle Created!");
        System.out.println();
    }

    public TriangleClass(int side1, int side2, int side3){      //Конструктор с инициализацией сторон
        System.out.println();
        System.out.println("Объект создан");
        System.out.println("Сторона 1 = " + side1);
        System.out.println("Сторона 2 = " + side2);
        System.out.println("Сторона 3 = " + side3);
        System.out.println();
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public TriangleClass(String color, int side1, int side2, int side3){        //Конструктор с инициализацией цвета и сторон
        super(color);
        System.out.println("Объект создан");
        System.out.println("Сторона 1 = " + side1);
        System.out.println("Сторона 2 = " + side2);
        System.out.println("Сторона 3 = " + side3);
        System.out.println("Цвет объекта : " + color);
        System.out.println();
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    int side1;
    int side2;
    int side3;

    @Override
    public void getLength() {     //Метод расчет периметра
        double length = side1 + side2 + side3;
        System.out.println("Периметр равен : " + length);
    }

    @Override
    public void getArea() {       //Метод расчет площади треугольника
        double p = (side1 + side2 + side3) / 2.0;     //Расчет полу периметра
        double area = Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));       //Расчет площади
        System.out.println("Площадь равна : " + area);

    }
}
