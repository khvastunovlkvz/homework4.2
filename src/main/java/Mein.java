public class Mein {

    public static void main(String[] args) {

        RectangleClass rectangle = new RectangleClass("Blue",10,10);

        rectangle.moveUp(150);
        rectangle.moveLeft(3);
        rectangle.getCoordinates();
        rectangle.getLength();

        RoundClass round = new RoundClass("Green", 5);

        round.getArea();
        round.getLength();
        round.moveDown(5);
        round.moveRight(3);
        round.getCoordinates();

        TriangleClass triangle = new TriangleClass("Yellow", 10, 10, 10);

        triangle.getArea();
        triangle.getLength();
    }
}
