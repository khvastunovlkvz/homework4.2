public abstract class Figure {

    int x = 0;
    int y = 0;
    String color;

    public Figure(){                                             //Пустой конструктор
        System.out.println();
        System.out.println("Запуск конструктора Figure");
        System.out.println("Конструктор Figure завершен");
        System.out.println();
    }

    public Figure(String color) {                              //Конструктор с инициализацией цвета
        this.color = color;
        System.out.println();
        System.out.println("Запуск конструктора Figure");
        System.out.println("Инициализация координат : x = " + x + "; y = " + y);
        System.out.println("Цвет : " + color);
        System.out.println("Конструктор Figure завершен");
        System.out.println();
    }

    public abstract void getLength();     //Абстрактный метод расчет периметра
    public abstract void getArea();       //Абстрактный метод расчет площади

    public void moveLeft(int left){                              //Метод сдвиг влево
        if (left >= 0) {        //Запрет ввода отрицательных чисел
            x -= left;
            System.out.println("Фигура сдвинута влево на: " + left);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }

    public void moveRight(int right){                            //Метод сдвиг вправо
        if(right >= 0) {        //Запрет ввода отрицательных чисел
            x += right;
            System.out.println("Фигура сдвинута вправо на: " + right);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }

    }
    public void moveUp(int up){                                  //Метод сдвиг вверх
        if (up >= 0) {          //Запрет ввода отрицательных чисел
            y += up;
            System.out.println("Фигура сдвинута вверх на: " + up);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }
    public void moveDown(int down){                              //Метод сдвиг вниз
        if (down >= 0) {        //Запрет ввода отрицательных чисел
            y -= down;
            System.out.println("Фигура сдвинута вверх на: " + down);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }
    public void getCoordinates(){                               //Метод инициализация конечных координат
        System.out.println("Текущее положение фигуры по координатам : x = " + x + "; y = " + y);
    }




}